# Architecture of a Booking/Billing System

## Overview

Given the light amount of details for this task, I'm making the assumption that
this is a booking system for something like a hotel, with start/end dates, discrete
customers, and so on.

This is a rough sketch of a possible booking system, without diving too much into
complexities such as delayed payment methods (credit, etc). Such considerations
would necessitate research into best practices, thorough conversations with the
client to gather requirements, and an appropriately long design phase.

Model classes below shall extend ActiveRecord-style objects for relevant helper
methods. See [Laravel's Eloquent ORM](https://laravel.com/docs/5.4/eloquent) for
an overview of how these types of systems work.

## Services Overview

1. Booking (Internal)
2. Billing (Internal)
3. Gateway (External)
   * Ties together the booking and billing systems behind an abstraction

## Service APIs

### Booking

#### Classes
```
Booking: {
    id : integer
    customerId : integer
    startDate : date
    endDate : date
}
```

### Billing

#### Classes
```
Payment: {
    id : integer
    billId : integer
    amount : float
}
```

```
Bill: {
    id : integer
    customerId : integer
    amount : integer
}
```

#### Service Methods
```
payBill(billId : integer)
```
* Throws exception if:
   * billId does not match a Bill in the system
   * billId already has a Payment object associated with it

### Gateway

#### Classes
```
Customer: {
    id : integer
    name : string
}
```

#### Service Methods
```
createBooking(customerName : string, startDate : date, endDate : date, amount : float) -> boolean
```
* Throws exception if any input does not validate or booking/billing/customer creation
fails

#### `createBooking` Pseudocode

```php
function createBooking(
    string customerName,
    Date $startDate,
    Date $endDate,
    float $amount) {

    // Validate that startDate is before endDate
    if ($startDate >= $endDate) {
        throw Exception('End date must be after start date');
    }

    $customer = new Customer();
    try {
        $customer->name = $customerName;
        $customer->save(); // Persist customer object for booking and billing
    } catch (Exception $e) {
        // Deleting $customer is unnecessary as it would not have been persisted in
        // an exception case.

        // Do not rethrow the internal exception. The client should not depend on
        // exceptions from classes or services it has no direct interaction with.
        throw new Exception('An error occurred creating this customer');
    }
    
    $booking = new Booking();
    try {
        $booking->customerId = $customer->id;
        $booking->startDate = $startDate;
        $booking->endDate = $endDate;
        $booking->save();
    } catch (Exception $e) {
        // Remove customer as the process failed and the customer will be useful
        $customer->delete();

        throw new Exception('An error occurred in creating your booking');
    }
    
    $bill = new Bill();
    try {
        $bill->customerId = $customer->id;
        $bill->amount = $amount;
        $bill->save();
    } catch (Exception $e) {
        $booking->delete();
        $customer->delete();
        
    }

    try {
        BillingService::payBill($bill->id);
    } catch (Exception $e) {
        $booking->delete();
        $bill->delete();
        $customer->delete();

        throw new Exception('An error occurred in billing for your booking');
    }
    
    return true;
}
```