'use strict';

const expect = require('chai').expect;

describe('Question A', () => {
    const longestConsecutiveNumberSubstring = require('./QuestionA');

    function integerToArray(integer) {
        return (""+integer).split('');
    }

    it('returns the longest substring of consecutive increasing numbers in an integer array', () => {
        expect(longestConsecutiveNumberSubstring(integerToArray(95213455))).to.equal('1345');
    });

    it('returns an empty string if array is empty', () => {
        expect(longestConsecutiveNumberSubstring([])).to.equal('');
    });

    // The reasoning behind this is that we're checking for consecutively increasing numbers. A single
    // number cannot be consecutively increasing because it is not greater than the number before it.
    it('returns an empty string if the input is only one number', () => {
        expect(longestConsecutiveNumberSubstring(integerToArray(3))).to.equal('');
    });

    it('returns an empty string if array has no consecutively increasing numbers', () => {
        expect(longestConsecutiveNumberSubstring(integerToArray(54321))).to.equal('');
    });

    it('returns the first of two equally long substrings of consecutive increasing numbers', () => {
        expect(longestConsecutiveNumberSubstring(integerToArray(123443678))).to.equal('1234');
    });
});