'use strict';

const expect = require('chai').expect;

describe('Question C', () => {
    // This is literally the same problem as Question A, but with letters; and JavaScript's type system makes comparing
    // characters work the same as numbers.
    const longestRepeatingSubstring = require('./QuestionA');

    it('returns the longest alphabetically increasing substring', () => {
        expect(longestRepeatingSubstring('nasdefghijkzeeioa')).to.equal('defghijkz');
    });

    it('returns a blank string if there is no increasing substring', () => {
        expect(longestRepeatingSubstring('zxy')).to.equal('');
    });

    it('returns an empty string if length is less than two', () => {
        expect(longestRepeatingSubstring('a')).to.equal('');
        expect(longestRepeatingSubstring('')).to.equal('');
    });
});