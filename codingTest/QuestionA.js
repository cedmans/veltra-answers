'use strict';

const longestConsecutiveNumberSubstring = require('./longestSubstring')((previous, current) => previous < current);

module.exports = longestConsecutiveNumberSubstring;