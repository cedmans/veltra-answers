'use strict';

const expect = require('chai').expect;

describe('Question B', () => {
    const longestRepeatingSubstring = require('./QuestionB');

    it('returns the longest repeating substring', () => {
        expect(longestRepeatingSubstring('11000011')).to.equal('0000');
    });

    it('returns the first of two equally long repeating substring sequences', () => {
        expect(longestRepeatingSubstring('11100011')).to.equal('111');
    });

    it('returns an empty string if there is no repeating substring sequence', () => {
        expect(longestRepeatingSubstring('10101010')).to.equal('');
    });

    it('returns an empty string if length is less than two', () => {
        expect(longestRepeatingSubstring('10')).to.equal('');
        expect(longestRepeatingSubstring('')).to.equal('');
    });
});