'use strict';

/**
 * A curried function which takes a character comparator and returns a function which will takes a string and use the
 * provided comparator to determine if a character is part of a substring, and then return the longest such substring
 * detected in the provided string.
 *
 * @param consecutiveComparator Comparator function whose prototype matches (previous, current) => boolean. If true, the
 * current character/value will be appended to the current longest substring.
 *
 * @return function A function which takes in a string and outputs the longest substring as defined by the
 * consecutiveComparator.
 */
const longestSubstring = consecutiveComparator => string => {
    // Assume that if there is only one item, there is no longest substring
    if (string.length < 2) return '';

    let longestSubstringLength = 0;
    let longestSubstring = '';

    let currentLongestSubstringLength = 1;
    let currentLongestSubstring = string[0];

    for (let i = 1; i < string.length; i++) {
        let currentCharacter = string[i];
        let lastCharacterInLongestSubstring = currentLongestSubstring[currentLongestSubstring.length - 1];

        if (consecutiveComparator(lastCharacterInLongestSubstring, currentCharacter)) {
            currentLongestSubstringLength++;
            currentLongestSubstring = `${currentLongestSubstring}${currentCharacter}`;
        } else {
            longestSubstring = currentLongestSubstringLength > longestSubstringLength ? currentLongestSubstring : longestSubstring;
            longestSubstringLength = Math.max(currentLongestSubstringLength, longestSubstringLength);

            currentLongestSubstringLength = 1;
            currentLongestSubstring = currentCharacter;
        }
    }

    return longestSubstringLength > 1 ? longestSubstring : '';
};

module.exports = longestSubstring;