'use strict';

const longestRepeatingSubstring = require('./longestSubstring')((previous, current) => previous === current);

module.exports = longestRepeatingSubstring;