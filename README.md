# Veltra Coding and Architecture Test Answers

## About

As requested, this repository is the culmination of my answers to the Veltra
Coding and Architecture tests.

## License and Copyright

See LICENSE.TXT for more information.

## Test Answers

### Coding Test

Coding test answers to the three provided questions are available in the
`./codingTest` directory.

#### Requirements

* NodeJS (Tested on v6.9.2)
* NPM (Tested on v4.0.5)

#### Building

From the root directory of the repository:

```bash
$ npm install
```

This will install the necessary requirements to run the Mocha/Chai tests.

#### Running Tests

```bash
$ npm run test
```

### Architecture Test

The entire architecture test answer is contained within `./architectureTest/index.md`.